using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    public static Game game;
    [SerializeField] private int StartLives;
    [SerializeField] private TextMeshProUGUI livesText;
    [SerializeField] private TextMeshProUGUI coinsText;
    [SerializeField] private GameObject mainMenu; 
    [SerializeField] private TextMeshProUGUI finalCoinsCount; 
    [SerializeField] private string firstLevelName = "Level1"; 
    private int PlayerLives;
    private int PlayerCoins = 0;

    private void Awake()
    {


        if (game == null)
        {
            game = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void LoseLive()
    {
        PlayerLives -= 10;
        if (PlayerLives <= 0)
        {
            PlayerLives = 0;
            GameOver();
        }
        ShowLives(PlayerLives);
        
    }

    private void GameOver()
    {
        Time.timeScale = 0f;
        mainMenu.SetActive(true);
        finalCoinsCount.text = PlayerCoins.ToString();
        
    }

    public void RestartGame()
    {
        PlayerLives = StartLives;
        PlayerCoins = 0;
        SceneManager.LoadScene(firstLevelName);
        ShowCoins(PlayerCoins);
        ShowLives(PlayerLives);
        mainMenu.SetActive(false);
        Time.timeScale = 1f;
    }
    public void ExitGame()
    {
        Application.Quit();
    }

    public void AddCoins(int amount)
    {
        PlayerCoins += amount;
        ShowCoins(PlayerCoins);
    }
    private void Start()
    {
        PlayerLives = StartLives;
        PlayerCoins = 0;
        ShowLives(PlayerLives);
        ShowCoins(PlayerCoins);
    }

    private void ShowLives(int amount)
    {
        livesText.text = amount.ToString();
    }
    private void ShowCoins(int amount)
    {
        coinsText.text = amount.ToString();
    }
    


}
























































































































































