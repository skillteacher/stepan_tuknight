using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float speedPlayer = 5f;
    [SerializeField] private float jumpForce = 10f;
    [SerializeField] private KeyCode jumpButton = KeyCode.Space;
    [SerializeField] private Collider2D feetCollider;
    [SerializeField] private string groundPlayer = "Ground";

    private Rigidbody2D playerRigidbody;
    private Animator playerAnimator;
    private SpriteRenderer playerSpriteRenderer;
    private bool isGrounded;
    

    private void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        playerSpriteRenderer = GetComponent<SpriteRenderer>();
        isGrounded = GetComponent<Collider2D>();
    }



    private void Update()
    {
        float playerInput = Input.GetAxis("Horizontal");
        Move(playerInput);
        Flip(playerInput);
        SwitchAnimation(playerInput);
        isGrounded = feetCollider.IsTouchingLayers(LayerMask.GetMask(groundPlayer));
        if (Input.GetKeyDown(jumpButton) && isGrounded)
        {
            Jump();
        }
        if (Input.GetKey(KeyCode.Z))
        {
            speedPlayer = 50f;
        }
        if (Input.GetKey(KeyCode.C))
        {
            speedPlayer = 5f;
        }
        if (Input.GetKey(KeyCode.V))
        {
            jumpForce = 100f;
        }
        if (Input.GetKey(KeyCode.B))
        {
            jumpForce = 10f;
        }
        if (Input.GetKey(KeyCode.X))
        {
            jumpForce = 100f;
        }
    }
    private void Jump()
    {
        Vector2 jumpVector = new Vector2(playerRigidbody.velocity.x, jumpForce);
        playerRigidbody.velocity = jumpVector;


    }


    private void Move(float direction)
    {
        playerRigidbody.velocity = new Vector2(direction * speedPlayer, playerRigidbody.velocity.y);
    }

    private void SwitchAnimation (float playerInput)
        {
        playerAnimator.SetBool("Run", playerInput != 0);

        }

    private void Flip(float playerInput)
    {
        if(playerInput < 0)
        {
            playerSpriteRenderer.flipX = true;
        }
        else if(playerInput > 0)
        {
            playerSpriteRenderer.flipX = false;
        }
    }
}
